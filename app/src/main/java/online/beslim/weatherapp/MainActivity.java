package online.beslim.weatherapp;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private final List<OneCity> allCity;
    private AllCityFragment allCityFragment;
    private OneCityFragment oneCityFragment;

    public MainActivity() {
        allCity = new ArrayList<>();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initCityList();
        initUi();
    }

    private void initCityList() {
        Resources resources = getResources();
        allCity.add(new OneCity(resources.getString(R.string.title_nsk), resources.getString(R.string.description_nsk)));
        allCity.add(new OneCity(resources.getString(R.string.title_msk), resources.getString(R.string.description_msk)));
        allCity.add(new OneCity(resources.getString(R.string.title_spb), resources.getString(R.string.description_spb)));
    }

    private void initUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        allCityFragment = new AllCityFragment();
        oneCityFragment = new OneCityFragment();

        replaceWithAllCityFragment();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener((view) -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void replaceWithAllCityFragment() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AllCityFragment.CITY_LIST, (Serializable) allCity);
        allCityFragment.setArguments(bundle);
        allCityFragment.setListener(this::replaceWithOneCityFragment);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, allCityFragment)
                .commit();
    }

    private void replaceWithOneCityFragment(int id) {
        Bundle args = new Bundle();
        args.putSerializable(OneCityFragment.CITY_ID, allCity.get(id));
        oneCityFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_container, oneCityFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (oneCityFragment.isAdded()) {
            replaceWithAllCityFragment();
            return;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        // super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(this, "wedqdqwdqdwrwe", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_search:
                return true;
            case R.id.action_add:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        int id = menuItem.getItemId();

        if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
}
