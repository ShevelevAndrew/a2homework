package online.beslim.weatherapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AllCityAdapter extends RecyclerView.Adapter<AllCityAdapter.ViewHolder> {

    private final List<OneCity> allCity;
    private final OnCityClickListener listener;

    AllCityAdapter(List<OneCity> allCity, OnCityClickListener listener) {
        this.listener = listener;
        this.allCity = allCity;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.one_city, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        OneCity oneCity = allCity.get(i);
        viewHolder.cityLabel.setText(oneCity.name());
        viewHolder.cityCoatOfArm.setImageResource(App.getImage(oneCity.name()));
        viewHolder.itemView.setOnClickListener(v -> listener.onClick(i));

    }

    @Override
    public int getItemCount() {
        return allCity.size();
    }

    public interface OnCityClickListener {
        void onClick(int cityId);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView cityCoatOfArm;
        TextView cityLabel;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            cityLabel = itemView.findViewById(R.id.one_city_label);
            cityCoatOfArm = itemView.findViewById(R.id.one_city_image);
        }
    }
}
